#+TITLE: Créer des environnements reproductibles avec R et Jupyter
#+SUBTITLE: ANF CNRS "Rendre sa recherche plus transparente, optimiser ses processus : data science en R et Python"
#+AUTHOR: Frédéric Santos
#+DATE: 25 novembre 2021
#+EMAIL: frederic.santos@u-bordeaux.fr
#+STARTUP: showall
#+OPTIONS: email:t toc:nil ^:nil
#+LATEX_HEADER: \usepackage[natbibapa]{apacite}
#+LATEX_HEADER: \usepackage[french]{babel}
#+LATEX_HEADER: \usepackage{a4wide}
#+LATEX_HEADER: \usepackage{mathpazo}
#+LATEX_HEADER: \usepackage[matha,mathb]{mathabx}
#+LATEX_HEADER: \usepackage{amsmath}
#+LATEX_HEADER: \usepackage{titlesec}
#+LATEX_HEADER: \titlelabel{\thetitle.\quad}
#+LATEX_HEADER: \usepackage[usenames,dvipsnames]{xcolor} % For colors with friendly names
#+LATEX_HEADER: \usepackage{minted}
#+LATEX_HEADER: \usepackage{mdframed}                    % Companion of minted for code blocks
#+LATEX_HEADER: \usepackage{fancyvrb}                    % For verbatim R outputs
#+LATEX_HEADER: \usemintedstyle{friendly} % set style if needed, see https://frama.link/jfRr8Lpj
#+LATEX_HEADER: \mdfdefinestyle{mystyle}{linecolor=gray!30,backgroundcolor=gray!30}
#+LATEX_HEADER: \BeforeBeginEnvironment{minted}{%
#+LATEX_HEADER: \begin{mdframed}[style=mystyle]}
#+LATEX_HEADER: \AfterEndEnvironment{minted}{%
#+LATEX_HEADER: \end{mdframed}}
#+LATEX_HEADER: %% Formatting of verbatim outputs (i.e., outputs of R results):
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{%
#+LATEX_HEADER:   fontsize = \small,
#+LATEX_HEADER:   frame = leftline,
#+LATEX_HEADER:   formatcom = {\color{gray!97}}
#+LATEX_HEADER: }
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{url}
#+LATEX_HEADER: %% For DOI hyperlinks in biblio:
#+LATEX_HEADER: \usepackage{doi}
#+LATEX_HEADER: \renewcommand{\doiprefix}{}
#+LANGUAGE: fr

* De la transparence à la reproductibilité (computationnelle)
Au cours de la formation, nous avons surtout vu jusqu'ici des manières de rendre sa recher plus /transparente/, notamment via l'usage de Jupyter Notebooks[fn::Ou bien sûr, de tout autre système de programmation lettrée, tel que Org mode ou Rmarkdown/knitr.]. Il est désormais fréquent que de tels /notebooks/ (ou /documents computationnels/) soient disponibles parmi le matériel supplémentaire en ligne d'un article de recherche [[citep:alonso-llamazares2021_AssessingIndividualPopulation][e.g.,::]], puisqu'ils permettent de documenter de façon très claire et très technique les analyses conduites dans l'article. Toutefois, même si de tels notebooks sont effectivement disponibles, il n'est pas garanti qu'ils puissent être ré-exécutés aisément par le lecteur :
- disposera-t-il de la même version de R ?
- disposera-t-il des mêmes versions de packages R ?
- d'autres problèmes encore plus subtils peuvent se poser, et sont liés aux paquets systèmes généraux.

Un des enjeux de la /reproductibilité computationnelle/ est donc de décrire, voire de fournir, un environnement logiciel précis, qui permettra de ré-exécuter le notebook et d'obtenir exactement les mêmes résultats --- même 5 ans plus tard, même sur un système d'exploitation différent, etc.

Il existe une assez large variété d'approches pour cela, certaines étant très légères, d'autres étant beaucoup plus techniques. Notons également qu'il existe une très abondante littérature scientifique sur le sujet, décrivant des méthodes et outils pour s'assurer de la reproductibilité computationnelle à long terme de nos analyses [[citep:alston2021_BeginnerGuideConducting,desquilbet2019_VersRechercheReproductible,stanisic2015_EffectiveGitOrgMode,munafo2017_ManifestoReproducibleScience,marwick2018_PackagingDataAnalytical,marwick2017_ComputationalReproducibilityArchaeological][e.g.,::]].

Notons que l'enjeu de la reproductibilité computationnelle, en plus d'être objectivement louable pour la science, présente également des bénéfices personnels clairs pour chaque chercheur en termes d'efficacité de travail citep:markowetz2015_FiveSelfishReasons, et est également amené à devenir à moyen terme un des éléments d'évaluation du travail académique citep:baker2020_ReproducibilityScientificResults.

* Approches pour la reproductibilité computationnelle avec R
Nous décrivons ci-dessous quelques approches possibles pour améliorer la reproductibilité des analyses conduites en R, par degré croissant de complexité technique.

** Le package ={groundhog}=
Durant la formation, nous avons utilisé le package R ={groundhog}= afin de s'assurer que nos notebooks utiliseraient certaines versions de packages R bien spécifiques. Ces versions sont simplement celles qui étaient disponibles sur le CRAN à une date précise. ={groundhog}= se charger alors de l'installation (si nécessaire) et du chargement en mémoire de ces versions spécifiques. L'utilisation plus avancée de package est détaillée sur son site internet, 

#+begin_center
\url{https://groundhogr.com/}
#+end_center

En particulier, ={groundhog}= permet aussi :
- de gérer certaines versions antérieures de R ;
- de gérer la version de ={groundhog}= lui-même, ce qui donne un aspect un peu "meta" à son fonctionnement.

Il s'agit d'une solution extrêmement légère et simple d'utilisation, qui représente déjà un excellent compromis entre facilité d'usage et sécurisation de la reproductibilité. Toutefois, il est encore de la responsabilité de l'utilisateur final du notebook de disposer de (la bonne version de) R sur son ordinateur, d'exécuter localement toutes les analyses sur sa machine, d'avoir éventuellement les (bonnes versions des) librairies système nécessaires à l'utilisation de certains packages R, etc.

** Le package ={renv}=
={renv}= (pour "\textbf{r}eproducible \textbf{env}ironments") est une solution très similaire à ={groundhog}=, partageant les mêmes fonctionnalités et la même philosophie d'utilisation, mais avec peut-être une facilité d'usage légèrement moins bonne. Là encore, il s'agit d'isoler certaines versions de packages R spécifiques dans un environnement dédié.

Les fonctionnalités complètes du package sont décrites sur son site officiel,

#+begin_center
\url{https://rstudio.github.io/renv/}
#+end_center

={renv}= présente donc essentiellement les mêmes limitations que ={groundhog}=, au sens où un certain nombre de points nécessaires à la reproductibilité des analyses continuent de dépendre de l'ordinateur de l'utilisateur final.

** Docker
Docker est la solution la plus sûre et efficace pour garantir la reproductibilité de long terme des analyses, au sens où il permet la capture système complète d'un environnement computationnel : nature et version du système d'exploitation, liste et version des librairies système, version de R et de l'ensemble des packages, éléments auxquels peut s'ajouter la version de LaTeX, Jupyter, etc.

Cette capture d'environnement se réalise par l'écriture d'un /Dockerfile/ spécifiant toutes les informations nécessaires ; et ce Dockerfile peut ensuite être transformé en /image/ (ou /conteneur/) qui contiendra l'exacte liste des éléments spécifiés dans le Dockerfile. L'utilisateur final doit alors simplement disposer de Docker sur son ordinateur : il peut ainsi ouvrir toute /image/ système pour récupérer l'environnement logiciel exact utilisé pour produire les résultats de l'article, et ainsi les reproduire à l'identique. Notons de plus que ces images peuvent être hébergées sur DockerHub pour être librement téléchargées :

#+begin_center
\url{https://hub.docker.com/}
#+end_center

De nombreux articles de recherche fournissent désormais des images Docker complètes permettant de reproduire la totalité des analyses, ainsi que le manuscrit lui-même si l'on inclut (par exemple) une distribution LaTeX dans l'image Docker [[citep:santos2020_ModernMethodsOld][e.g.,::]].

Docker assure donc (théoriquement) à 100% la reproductibilité de long terme des analyses, mais reste une solution assez lourde :
- la taille des images système à télécharger par l'utilisateur final peut être assez importante dans certains cas ;
- l'utilisateur final doit lui-même disposer de Docker sur sa machine, et continuer d'exécuter localement les analyses ;
- l'écriture de Dockerfiles nécessite un peu d'apprentissage, et reste en tout cas une solution moins évidente à mettre en \oe uvre que la simple utilisation des packages ={groundhog}= ou ={renv}=.

** Binder
Binder est une solution basée sur Docker, mais vise à en résoudre certains inconvénients. En effet, l'idée générale de Binder est d'exécuter un notebook /directement en ligne sur un serveur distant/, lequel serveur dispose lui-même d'un environnement logiciel précis, spécifié par un Dockerfile. Binder décharge donc l'utilisateur final de la tâche de disposer de Docker sur sa machine, et d'y télécharger l'image qu'il souhaite lancer.

À la place, la démarche usuelle consiste à :
1. Créer un dépôt GitLab (ou GitHub, ou autre) contenant les notebooks, scripts, données[fn::Les données peuvent également être hébergées ailleurs, par exemple sur Zenodo ; mais les notebooks doivent alors explicitement charger les données depuis l'URL en question.], et tout autre matériel utile aux analyses.
2. Spécifier, d'une manière ou d'une autre (soit avec un Dockerfile, soit avec d'autres plus solutions plus légères que nous verrons ci-après), le contenu de l'environnement logiciel nécessaire à la bonne exécution des analyses.
3. Lier le dépôt Git au site [[https://mybinder.org/][~https://mybinder.org/~]], afin que Binder crée l'image système requise, et fournisse un lien pour l'exécution des analyses.
4. Il suffit alors d'ajouter un bouton Binder sur la page d'accueil du dépôt Git : l'utilisateur final n'a plus qu'à cliquer dessus pour lancer et exécuter aisément toutes les analyses depuis son navigateur internet, à partir d'un serveur distant de Binder (cf. Fig. ref:fig-mybinder, associée à la publication de cite:boettiger2018_NoiseKnowledgeHow).

#+CAPTION: Un dépôt GitHub lié à Binder : l'utilisateur final peut alors simplement cliquer surle bouton "Launch Binder" afin de reproduire aisément la totalité des analyses. Dépôt accessible à l'adresse \url{https://github.com/cboettig/noise-phenomena}. label:fig-mybinder
#+ATTR_LATEX: :width \textwidth
[[./images/binder.png]]

* En pratique : lier un dépôt GitLab à Binder
Dans cette section, nous allons créer un nouveau dépôt GitUB, y déposer une feuille de données ainsi qu'un notebook R contenant quelques "analyses" très élémentaires, et lier ce dépôt GitUB à Binder en spécifiant l'utilisation d'une version de R spécifique, et de versions de packages spécifiques. Nous n'entrerons pas dans les détails de l'utilisation ou de la rédaction d'un Dockerfile, mais une documentation complète est disponible à la fois sur les sites Internet de Docker (\url{https://docs.docker.com/}) et de Binder (\url{https://mybinder.readthedocs.io/en/latest/index.html}). Nous verrons qu'il existe, pour les cas simples comme celui que nous créerons ici, des moyens très simples d'arriver à nos fins sans nécessairement passer par l'écriture d'un Dockerfile.

** Création d'un dépôt GitUB vierge
1. Rendez-vous sur GitUB (\url{https://gitub.u-bordeaux.fr/}), puis créez un dépôt /public/ vierge, que vous pourrez par exemple appeler =mybinder_example_R=.
2. Clonez-le en local, en utilisant la méthode SSH, sur un emplacement quelconque de votre ordinateur.

** Réalisation des analyses statistique
1. Localement, créez un sous-dossier =data= dans votre répertoire, dans lequel vous placerez la feuille de données suivante : \url{https://link.infini.fr/maxwellcsv}.
2. À la racine (par exemple !) de votre répertoire, créez un nouveau Jupyter Notebook R, nommé =index.ipynb=, et effectuez quelques opérations très simples sur ce jeu de données (par exemple, le charger, le résumer, faire un ou deux graphiques ou tests arbitraires, et représenter une ACP). Vérifiez la bonne exécution des analyses du notebook.
3. Faites un /push/ vers le dépôt GitUB distant.

** Organisation du dépôt
1. Toujours localement, créez à la racine du répertoire un fichier (impérativement) nommé =runtime.txt=. Ce fichier devra contenir uniquement le texte suivant :
   #+begin_src 
r-4.0-2020-05-01
   #+end_src
   Nous spécifions ainsi, par cette syntaxe simple, que nos analyses doivent être réalisées avec un environnement R tel que disponible en date du premier mai 2020, ce qui correspond à une version 4.0 de R ainsi qu'à des versions de packages spécifiques. Notez que cela nous dispense de l'utilisation de ={groundhog}= dans le notebook principal, puisque l'image système finale utilisera nécessairement les versions de packages R disponibles en date du 01/05/2020.
2. Créez également à la racine du répertoire un script =install.R= qui contiendra uniquement les instructions nécessaires à l'installation des packages requis. Par exemple, il pourra contenir les trois instructions suivantes si vous avez besoin des trois packages correspondants dans vos analyses :
   #+begin_src R :eval no :exports code
install.packages("FactoMineR")
install.packages("here")
install.packages("missMDA")
   #+end_src
3. Réalisez à nouveau un /push/ de ces nouveaux fichiers vers le dépôt GitUB distant.

** Liaison du dépôt Git avec Binder
Pour finir, rendez-vous sur le site \url{https://mybinder.org/} afin de procéder à la création d'une image simple pour le logiciel R (avec ses versions spécifiées), et à l'obtention d'un badge Binder pour votre dépôt. Il suffit de remplir les informations nécessaires comme en Figure ref:fig-mybinder-info. Après validation, l'image Docker nécessaire sera créée, ce qui pourra prendre un peu de temps.

#+CAPTION: Un exemple de formulaire mybinder.org pour un dépôt GitUB. label:fig-mybinder-info
#+ATTR_LATEX: :width \textwidth
[[./images/mybinder.png]]

** Affichage du badge Binder sur le dépît GitUB
Pendant ce temps, vous pouvez créer un fichier =README.md= à la racine de votre dépôt, et y insérer le lien (fourni par mybinder.org, cf. partie inférieure de la Figure ref:fig-mybinder-info) permettant d'obtenir votre badge. Après un dernier push (et après le temps nécessaire à la création de l'image Docker !), vous obtenez ainsi un lien cliquable permettant à tout visiteur de reproduire vos analyses, directement sur les serveurs de Binder, et en utilisant un environnement logiciel spécifique.

* Biblio                                                             :ignore:
bibliography:~/PACEA_MyCore/complete_biblio.bib
bibliographystyle:apacite
